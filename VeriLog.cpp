//
// Created by stree on 22/06/2019.
//

#include <fstream>
#include <sstream>
#include "VeriLog.h"
#include <algorithm>
#define INT_MAX INT8_MAX

VeriLog::VeriLog() {
    /*i_o io;
    for (int i = 0; i < 4; ++i) {
        _inputVect.push_back(io);
    }
    _inputVect[0].name = "a";
    _inputVect[1].name = "b";
    _inputVect[2].name = "c";
    _inputVect[3].name = "d";*/

    Min_Path = INT_MAX;
    Max_Path = -1;
}

VeriLog::~VeriLog() {

}

int VeriLog::readAndCreate(string fileName)  {
    string word = "start", temp;
    ifstream file;
    bool flag = false;
    bool composito = false;
    in input;
    genout output;
    int k=0, z=0, n;

    if(fileName.find(".txt")==string::npos)                                         //Corregge il nome se non presente il .txt
        fileName+=".txt";

    file.open(fileName);

    if(!file.is_open()) {
        cerr << "Error: File could not be open." << endl;
        return -1;
    }

    while(!file.eof()) {               //???????????????? Arrivare sino ad endmodule non a eof
        if(!flag)
        {
            file>>word;                 //caso in cui ci sia una parola chiave non valida
        }
        flag = false;

        if (word == "module") {
            flag= true;
            file>>word;
            _circuitName=word;
            if(_circuitName.find("COMPOSITO")!=string::npos){
                composito = true;
            }
        }
        else{
            if (word == "input") {
                file >> word;
                flag=true;
                while ((word != "output")&&(word != "input")&&(word!=");")) {
                    eraseComma(word);
                    n = checkIsVector(word);
                    for (int i = 0; i < n; i++) {
                        word = renameVect(word, i);
                        input.name = word;
                        input.value = false;
                        if(!composito)
                            _inputVect.push_back(input);
                        else
                            _inputVectComposito.push_back(input);
                    }
                    file >> word;
                }
            }
            else{
                if (word == "output") {
                    flag=true;
                    file >> word;
                    while ((word != "output")&&(word != ");")&&(word!="input")) {
                        eraseComma(word);
                        n = checkIsVector(word);
                        for (int i = 0; i < n; i++) {
                            word = renameVect(word, i);
                            output.name = word;
                            if(!composito)
                                _generalOutput.push_back(output);
                            else
                                _generalOutputComposito.push_back(output);
                        }
                        file >> word;
                    }
                }
                else{
                    if (word == "assign") {
                        flag=true;                                          // CONTROLLO CHE ASSIGN VADA ALLA PAROLA GIUSTA!! Matteo
                        file>>word;
                        bool exit = false;
                        if(!composito){
                            for(int j=0; j< _generalOutput.size() && !exit ;j++)                                                   //Bisogna associare parola all' output corrispondente
                            {
                                if(word == _generalOutput[j].name)
                                {
                                    exit = true;
                                    file.ignore(256,'=');
                                    getline(file, temp, '\n');
                                    _generalOutput[j].equation = temp;
                                }
                            }
                        }
                        else{
                            for(int j=0; j< _generalOutputComposito.size() && !exit ;j++)                                                   //Bisogna associare parola all' output corrispondente
                            {
                                if(word == _generalOutputComposito[j].name)
                                {
                                    exit = true;
                                    file.ignore(256,'=');
                                    getline(file, temp, '\n');
                                    _generalOutputComposito[j].equation = temp;
                                }
                            }
                        }
                    }
                    else{
                        if(word[0] == 'F' && word[1] == 'F'){
                            if(!composito){
                                _generalFlipflop.emplace_back();                        //Da verificare il funzionamento ALE.
                                _flipflopToOutputVect.emplace_back();
                                _generalFlipflop[k].name = word;
                                _generalFlipflop[k].previousValue = false;              //Per inizializzare previous
                                _flipflopToOutputVect[k].name = word;                   //Creando una speculare
                                file.ignore(256,'=');
                                getline(file, temp, '\n');
                                _generalFlipflop[k].equation = temp;
                                k++;
                            }
                            else{
                                _flipflopComposito.emplace_back();                        //Da verificare il funzionamento ALE.
                                _flipflopComposito[z].name = word;
                                file.ignore(256,'=');
                                getline(file, temp, '\n');
                                _flipflopComposito[z].equation = temp;
                                z++;
                            }
                        }
                        else{
                            if(word == "instance"){             // instance (.a = m, .b = n, .c = a, .x = FF1)
                                getline(file, temp, '\n');
                                _wireComposito = instanceReader(temp);

                                // instance (.a = m, .b = n, .c = a, .x = FF1)
                            }
                        }
                    }
                }
            }
            n=0;
        }
    }
    file.close();

    setOutputVect();                        // chiuso il file eseguo questa funzione per sistemare gli oggetti GRAFO.
    setpartialflipflop();

    return 1;     // FINO A QUI FACCIO ACQUISIZIONE DATI!!
}

vector<string> VeriLog::getData_Simulation(ifstream &inputFile) {                           //Bisognerebbe creare un vettore n_esimo di oggetti in modo da salvare tutti i dati di simulazione
    //oppure leggere e simulare una riga per volta
    ofstream outputFile;
    string line;
    vector<string> dataOutputFile(2);                             //Valutare se si può fare con uno stream
    bool temp, flag = true;
    bool output;
    char choice;
    char save;
    ostringstream outputLine;
    string temp2;

    //Riempimento prime righe vettore inputFile
    dataOutputFile[0] += "Input";
    for (int l = 0; l < _inputVect.size(); ++l) {
        dataOutputFile[0] += '\t';
    }
    dataOutputFile[0] += " Output";

    for (int l = 0; l < _inputVect.size(); ++l) {
        dataOutputFile[1]+=_inputVect[l].name + '\t';
    }

    dataOutputFile[1]+= "| ";

    for (int l = 0; l < _outputVect.size(); ++l) {
        dataOutputFile[1]+=_outputVect[l].name + '\t';
    }
    //Fine formattazione standard prime 2 linee

    if(!inputFile.is_open()) {
        //cerr << "Error: input inputFile could not be open." << endl;

        //Ci vuole una exit
    }
    else{
        while (!inputFile.eof()) {
            inputFile >> temp2;

            for (int i = 0; i < _inputVect.size(); i++) {
                temp = temp2[i] != '0';
                _inputVect[i].value = temp;

                outputLine << noboolalpha << _inputVect[i].value << "\t";
                line += outputLine.str();

                outputLine.str("");
            }

            outputLine << "| ";
            line += outputLine.str();
            outputLine.str("");


            for (int j = 0; j < _outputVect.size(); ++j) {                              //NON HO CONTROLLATO SE FUNZIONA. Ale
                if(!_outputVect[j].grafo.GetLoopRequired()){
                    _outputVect[j].grafo.setter(_inputVect);
                    _outputVect[j].value = _outputVect[j].grafo.calculator(dataPower);
                    _outputVect[j].grafo.reborn();
                }
                else{
                    _outputVect[j].grafo.setter(_inputVect);
                    _outputVect[j].grafo.setter(_generalFlipflop);
                    _outputVect[j].value = _outputVect[j].grafo.calculator(dataPower);
                    _outputVect[j].grafo.reborn();
                }


                outputLine << noboolalpha << _outputVect[j].value << "\t";
                line += outputLine.str();
                outputLine.str("");
            }     //NEWWW MATTEO HO ATTIVATO QUESTE RIGHE DI CODICE COMMENTATE E HO CHIAMATO PARTIAL

            partialFlipflop(_inputVect);
            for (int k = 0; k < _generalFlipflop.size(); ++k) {
                _generalFlipflop[k].previousValue = _generalFlipflop[k].actualValue;
            }

            dataOutputFile.push_back(line);
            line.clear();
        }
        inputFile.close();


    }
    return dataOutputFile;
}


int VeriLog::checkIsVector(const string &to_check) {
    string dim_string = "0";                                        //Inizializzato a '0' (perchè non modifica la dimensione) per poter eseguire il check finale ">1"
    int vectDim, i=0;
    bool flag = false;
    int control = 0;
    istringstream number;

    //for (int i = 0; i < to_check.size(); i++) {
    while(i < to_check.size() && !flag){
        if (to_check[i] == '[') {                                   //Controllo alla prima quadra
            control=1;

            while(i<to_check.size() && !flag){
                i++;
                if (isdigit(to_check[i]))
                    dim_string += to_check[i];
                else {
                    if (to_check[i] == ']'){    /*&& (to_check[i++] == ' ' || to_check[i++] == ',' || to_check[i++] == '\n')*/ //Controllo che il vettore concluda con spazio, virgola o a capo.
                        flag = true;
                        control=2;
                    }
                }
            }
        }
        else {
            i++;
        }
    }

    if(control==1){
        cerr << "Error: Input name not allowed." << endl;
        return -1;
    }
    number.str(dim_string);
    number >> vectDim;              //oppure usare stringstream(number) >> vectDim;
    if (flag && vectDim > 1) {
        return vectDim;
    } else {
        return 1;                   //NON è un vettore ma una var. singola e ritorna una dimensione compatibile con una variabile unitaria
    }
}

void VeriLog::eraseComma(string &to_modify) {
    if(to_modify.back() == ','){
        to_modify.pop_back();
    }
}

string VeriLog::renameVect(string word, int &n) {
    string new_word;
    int i=0;
    ostringstream number;
    size_t pos;

    if(word.find('[')!=string::npos){
        while(i < word.size() && word[i]!='['){
            if (word[i] != '[') {                                        //Controllo alla prima quadra
                new_word+=word[i];
            }
            i++;
        }
        number << n;
        new_word+=("[" + number.str() + "]");

        return new_word;
    }
    else
        return word;
}

string VeriLog::getOutputVectEquation(const string &outputName) {
    int i=0;

    while(i<_outputVect.size()){
        if(_outputVect[i].name == outputName)
            return _outputVect[i].equation;
    }
    return "error";
}

void VeriLog::setOutputVect() {
    out output;
    for (int i = 0; i < _generalOutput.size() ; ++i) {
        output.name = _generalOutput[i].name;
        output.value = false;
        _outputVect.push_back(output);
        _outputVect[i].grafo.equationParser(_generalOutput[i].equation,_generalOutput[i].name,_generalOutput[i].name,_inputVect,_generalOutput,_generalFlipflop);
    }
}

int VeriLog::getInputVectSize(){
    return _inputVect.size();
}

int VeriLog::getData_PowerAnalisis(ifstream &inputFile){

    string line;
    istringstream getData;
    char ignore;
    bool flag = false;
    power add;

    if(!inputFile.eof()){
        getline(inputFile,line);                                             //Si può fare senza sprecare una variabile?
    }

    while (!inputFile.eof() && !flag) {
        getline(inputFile, line, ';');
        if (line == "OR" ||line == "AND" ||line == "XOR" ||line == "NOR" ||line == "NAND" ||line == "XNOR" ||line == "NOT" ||line== "FF") {
            add.name= line;
            getline(inputFile, line, '\n');
            getData.str(line);
            getData >> add.from0to1>> ignore >> add.from1to0;                      //Occhio che se metti tre input funziona comunque
            dataPower.push_back(add);
            if(ignore!=';')
                flag=true;

            getData.clear();
        }
        else{
            flag = true;
        }
    }

    if (flag){
        //cerr << "Error: File's format not suitable." << endl;
        return -1;
    }

    inputFile.close();
    return 0;
}

vector<pair<string, string>> VeriLog::instanceReader(string to_read) {
    size_t pos = 0,i;
    int j=0;
    vector<pair<string, string>> to_return;

    while(pos!=string::npos){
        pos = to_read.find('.', pos);
        if(pos!=string::npos){
            to_return.emplace_back();
            i = pos+1;
            while(to_read[i]!=' '){
                to_return[j].first += to_read[i];
                i++;
            }
            i=i+3;
            while(isalnum(to_read[i])){
                to_return[j].second += to_read[i];
                i++;
            }
            j++;
            pos++;
        }
    }
    return to_return;

    /*
    // Example: (.a = m, .b = n, .c = a, .x = FF1)

    to_return[0].first = a;
    to_return[0].second = m;
    to_return[1].first = b;
    to_return[1].second = n;
    to_return[2].first = c;
    to_return[2].second = a;
    to_return[3].first = x;
    to_return[3].second = FF1;

     */
}

void VeriLog::partialFlipflop(vector<in> to_setInput) {
    for (int i = 0; i < _flipflopToOutputVect.size(); ++i) {
        //_flipflopToOutputVect[i].grafo.equationParser(_generalFlipflop[i].equation, _generalFlipflop[i].name,_generalFlipflop[i].name, to_setInput, _generalOutput, _generalFlipflop);
        _flipflopToOutputVect[i].grafo.setter(to_setInput);
        _flipflopToOutputVect[i].grafo.setter(_generalFlipflop);
        _generalFlipflop[i].actualValue = _flipflopToOutputVect[i].grafo.calculator(dataPower);
        _flipflopToOutputVect[i].grafo.reborn();
    } //NEWWWWW MATTEO
}

vector<vector<string>> VeriLog::logicCones() {
    vector< vector<string> > dataReturn;

    for (int i = 0; i < _outputVect.size(); ++i) {
       dataReturn.emplace_back();
        for (int j = 0; j < _outputVect[i].grafo.getCircuitoSize(); ++j) {
            //Controlla che sia un input e che non sia un input ripetuto (cercando la NON-presenza di "$$")
            if(_outputVect[i].grafo.getCircuito()[j].is_input && _outputVect[i].grafo.getCircuito()[j].name.find("$$")== string::npos){
                dataReturn[i].push_back( _outputVect[i].grafo.getCircuito()[j].name) ;
            }
        }
    }
    return dataReturn;
}

const vector<string> VeriLog::getOutputVectName() const {
    vector<string> dataReturn;
    for (int i = 0; i < _outputVect.size(); ++i) {
        dataReturn.push_back(_outputVect[i].name);
    }
    return dataReturn;
}

float VeriLog::getOutputVectTotalPower() const {
    float sum = 0;
    for (int i = 0; i < _outputVect.size(); ++i) {
        sum+= _outputVect[i].grafo.getTotalPower();
    }
    return sum;
}

vector<out> VeriLog::getOutputVect() const {
    return _outputVect;
}

void VeriLog::MaxAndMinPath()
{
    vector <string> temp;
    string search;
    int p;
    int counter;

    for (counter = 0; counter < _outputVect.size(); counter++)
    {
        for (int j = 0; j < _outputVect[counter].grafo.getCircuito().size(); ++j) {

            if (_outputVect[counter].grafo.getCircuito()[j].is_input)
            {
                int z = 0, i = 0;

                search = _outputVect[counter].grafo.dollarConverter(_outputVect[counter].grafo.getCircuito()[j].name);
                p = j;
                temp.clear();

                while ((_outputVect[counter].grafo.getCircuito()[p].nextnode != -1) && (i < _outputVect[counter].grafo.getCircuito().size())){
                    p = _outputVect[counter].grafo.getCircuito()[p].nextnode;
                    temp.push_back(_outputVect[counter].grafo.getCircuito()[p].logicInfo);
                    z++;
                    i++;
                }

                if(z< Min_Path)
                {
                    Min_Path = z;
                    Min_Search = search;
                    Min_Vec = temp;
                }
                if(z> Max_Path)
                {
                    Max_Path = z;
                    Max_Search = search;
                    Max_Vec = temp;
                }
            }
        }
    }
}

string VeriLog::getMaxSearch() const {
    return Max_Search;
}

string VeriLog::getMinSearch() const {
    return Min_Search;
}

int VeriLog::getMaxPath() const {
    return Max_Path;
}

int VeriLog::getMinPath() const {
    return Min_Path;
}

vector<string> VeriLog::getMinVec() const {
    return Min_Vec;
}

vector<string> VeriLog::getMaxVec() const {
    return Max_Vec;
}

void VeriLog::setpartialflipflop() {
    for (int i = 0; i <_flipflopToOutputVect.size() ; ++i) {
        _flipflopToOutputVect[i].grafo.equationParser(_generalFlipflop[i].equation, _generalFlipflop[i].name,_generalFlipflop[i].name, _inputVect, _generalOutput, _generalFlipflop);
    }
}