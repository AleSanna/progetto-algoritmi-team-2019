//
// Created by stree on 22/06/2019.
//

#ifndef PROGETTO_GIORNO_DEBUG_VERILOG_H
#define PROGETTO_GIORNO_DEBUG_VERILOG_H

#include <string>
#include <iostream>
#include <vector>
#include "Graph.h"

using namespace std;

struct out{
    string name;
    bool value;
    string equation;
    Graph grafo;
};

class VeriLog{
public:
    VeriLog();
    ~VeriLog();

    //Functions that work on Input/Output files
    int readAndCreate(string fileName);
    vector<string> getData_Simulation(ifstream &inputFile);
    vector<vector<string>> logicCones();
    int getData_PowerAnalisis(ifstream &inputFile);
    void MaxAndMinPath();
    void setpartialflipflop();
    //Getter and Setter
    string getOutputVectEquation(const string &outputName);
    float getOutputVectTotalPower() const;
    const vector<string> getOutputVectName() const;
    int getInputVectSize();
    void setOutputVect();
    vector<out> getOutputVect() const;

    //Getter for Path
    string getMaxSearch() const;
    string getMinSearch() const;
    int getMaxPath() const;
    int getMinPath() const;
    vector<string> getMinVec() const;
    vector<string> getMaxVec() const;

protected:
    //Data from Files
    string _circuitName;
    vector<power> dataPower;
    vector<genout> _generalOutput;
    vector<flip> _generalFlipflop;

    vector<out> _outputVect;
    vector<out> _flipflopToOutputVect;

    vector<in>_inputVect;

    //Circuiti Compositi
    vector<flip> _flipflopComposito;
    vector<pair<string, string>> _wireComposito;
    vector<genout> _generalOutputComposito;
    vector<in> _inputVectComposito;

    //Path Data
    string Max_Search, Min_Search;
    int Max_Path, Min_Path;
    vector <string> Min_Vec;
    vector <string> Max_Vec;

    //Function just for internal usage
    int checkIsVector(const string &to_check);
    void eraseComma(string &to_modify);
    string renameVect(string word, int &n);
    vector<pair<string, string>> instanceReader (string to_read);

    //Calcolo FlipFlop
    void partialFlipflop(vector<in> to_setInput);


    //int _error;

};


#endif //PROGETTO_GIORNO_DEBUG_VERILOG_H
