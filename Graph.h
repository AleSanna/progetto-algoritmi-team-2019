//
// Created by stree on 22/06/2019.
//

#ifndef PROGETTO_GIORNO_DEBUG_GRAPH_H
#define PROGETTO_GIORNO_DEBUG_GRAPH_H

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

//#define X -1                                         Si potrebbe usare una define e cambiare tutti i valori a interi

struct power{
    string name;
    float from0to1;
    float from1to0;
};

struct genout{
    string name;
    string equation;
};

struct flip{
    string name;
    string equation;
    bool previousValue;
    bool actualValue;
};

struct in {                                     //Diventa ereditaria
    string name;
    bool value;
};

struct node{
    string name;
    bool is_input;     //mi dice se è un input
    bool is_next;
    bool is_loop;
    bool inputValue1;
    bool inputValue2;
    bool outputValue;
    string logicInfo;
    int nextnode;
    bool is_x;

    //struct node *ptrNext;
};

class Graph {
public:

    Graph();
    ~Graph();

    //Create Graph
    void insertNodeLoop(const string &name, const bool &is_input, const bool &is_loop);
    void insertNode(const string &name, const bool &is_input);
    void insertNode(const string &name, const bool &is_input, const string &logicInfo);
    void insertLink(const string &start, const string &end);

    //Calculator
    bool calculator(vector<power> dataPower);    //possiede al suo interno anche il calcolo della potenza Matteo

    //Equation Parser ho aggiunto dei parametri name che serve per evitare loop infiniti e il vettore di flipflop matteo
    bool LoopFinder(string to_find ,string equation);
    string equationParser(string equation, string name,string inhibitor, vector<in> _inputvect, vector<genout> GeneralOutput, vector<flip> _flipflop);
    int powerCalculator(int temp, vector<power> dataPower);
    string checkPresence(string to_search);                                           //Trasforma da X a X$$n dove n è il numero per il quale si ripete
    string dollarConverter(const string &to_check);                                   //Letto il valore trasformato nel formato dollaro, lo riconverte nell' originale

    bool logicOperation(bool op1, bool op2, string logicGate);

    bool GetLoopRequired() const;

    // setter uno per input e uno per i FF con loop matteo
    void reborn();
    void setter(vector<in> _inputVect);      // per evitare che parser si chiami all' infinito quando trova un loop faccio aggiungere
    void setter(vector<flip> _flipflop);     // un nodo con il nome del flipflop e quindi previous value deve essere inizializzato     MATTEO
    // il valore incognito esempio (x= x and c) è assunto come false. per capire se ci sono loop nel circuito ho creato la variabile looprequired

    //Getter
    int getCircuitoSize() const;
    vector<node> getCircuito() const;
    float getTotalPower() const;

protected:
    //Graph vector
    vector<node> circuito;
    float totalPower;
    bool loopRequired = false;

    //Function just for internal usage
    int logicCounter(const string &equation);
    int parenthesisCounter(string equation);
    string replaceComb(const string &equation1, string to_find, string to_sub);
};

#endif //PROGETTO_GIORNO_DEBUG_GRAPH_H
